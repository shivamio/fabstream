import { Component, OnInit } from '@angular/core';
import { UploadService } from '../../services/upload.service';
import { AuthService } from '../../services/auth.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Socket } from 'ngx-socket-io';
import { Router } from '@angular/router';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  upPercentage: string;
  showProgress: Boolean;
  barPercentage: string;
  showProcessing: Boolean;
  uid: string;
  videoId: string;
  constructor(private _up: UploadService, private socket: Socket, private _auth: AuthService, private _router:Router) { }

  ngOnInit(): void {
    this.uid = this._auth.getUid();
    this.showProgress = false;
    this.showProcessing = false;
    this.upPercentage = '0';
    this.socket.on(this.uid, (data) => {
      if (data.event == 'video-process-start') {
        this.showProgress = false;
        this.showProcessing = true;
        this.videoId = data.videoId;
        console.log('Video Process started.. Please wait. VideoId: ' + this.videoId);
      }
      if (data.event == 'video-process-complete') {
        this._router.navigate(['/edit-video', this.videoId]);
        console.log('Video processing complete. Go to studio');
      }
    })
  }
  fileUpload(event) {
    this.showProgress = true;
    let fileList: FileList = event.target.files;
    let file: File = fileList[0];
    this._up.videoUpload(file).subscribe(event => {
      if (event.type == HttpEventType.UploadProgress) {
        const percentDone = Math.round(100 * event.loaded / event.total);
        this.upPercentage = String(percentDone);
        this.barPercentage = 'width: ' + this.upPercentage + '%'
        console.log(`File is ${percentDone}% loaded.`);
      } else if (event instanceof HttpResponse) {
        console.log('File is completely loaded!');
      }
    }, (err) => {
      console.log("Upload Error:", err);
    }, () => {
      console.log("Upload done");
    })
  }
}

import { Component, OnInit, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { VideoListServiceService } from '../../services/video-list-service.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-watch',
  templateUrl: './watch.component.html',
  styleUrls: ['./watch.component.css']
})
export class WatchComponent implements OnInit, OnDestroy {
  private sub:any;
  videoId:string;
  isValid:Boolean;
  thisVidResponse:any;
  thisVideo:any;
  constructor(private router:Router, private route:ActivatedRoute, private vls:VideoListServiceService, private ds:DataService) { }

  ngOnInit(): void {
    this.isValid = false;
    this.sub = this.route.params.subscribe(params=>{
      this.videoId = params['id'];
      if(this.videoId == undefined || this.videoId == null){
        //No id provided.
        this.router.navigate(['/']);
      }else{
        this.vls.fetchVideoMeta(this.videoId).subscribe(response=>{
          this.thisVidResponse = response;
          if(this.thisVidResponse.success){
            this.isValid = true;
            this.thisVideo = this.thisVidResponse.meta;
            this.ds.iv(this.videoId).subscribe(resp=>{
              let uvResp:any = resp;
              if(uvResp.success){
                console.log('Views incremented');
              }
            });
          }else{
            this.router.navigate(['/']);
          }
        })
      }
    })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}

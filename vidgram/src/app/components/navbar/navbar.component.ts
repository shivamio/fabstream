import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn:Boolean;
  username:string;
  constructor(private _auth:AuthService, private _router:Router) { }

  ngOnInit(): void {
    this.checkLoggedIn();
    this._auth.stateChange.subscribe(state=>{
      this.checkLoggedIn();
    })
  }

  checkLoggedIn(){
    if(this._auth.loggedIn()){
      this.isLoggedIn = true;
      this.username = this._auth.getUsername();
    }else{
      this.isLoggedIn = false;
    }
  }

  logout(){
    this._auth.logout();
    this._router.navigate(['/']);
  }

}

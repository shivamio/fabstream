import { Component, OnInit } from '@angular/core';
import { VideoListServiceService } from '../../services/video-list-service.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  thisVideo:any;
  fetchOneResponse:any;
  hasFetchOneError:Boolean;
  fetchedOne:Boolean;
  fetchOneError:string;
  constructor(private vls:VideoListServiceService) { }

  ngOnInit(): void {
    this.hasFetchOneError = false;
    this.fetchedOne = false;
    this.vls.fetchVideoMeta('L5_zECLlr0FilnFO').subscribe(
      data=>{
        this.fetchOneResponse = data;
        console.log(this.fetchOneResponse);
        if(this.fetchOneResponse.success){
          this.hasFetchOneError = false;
          this.fetchedOne = true;
          this.thisVideo = this.fetchOneResponse.meta;
        }else{
          this.hasFetchOneError = true;
          this.fetchOneError = this.fetchOneResponse.msg;
        }
      }
    )
  }

  watchVideo(id){
    console.log('User wants to watch video: '+id);
  }

  visitChannel(username){
    console.log('User wants to visit channel: '+username);
  }

}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  haveError:Boolean;
  errorText:string;

  identifier:string;
  password:string;
  loginResponse:any;
  constructor(private _router:Router, private _auth:AuthService) { }

  ngOnInit(): void {
    this.haveError = false
  }

  signin(){
    let loginObj = {
      identifier:this.identifier,
      password:this.password
    }
    if(this.validateLogin(loginObj)){
      this._auth.signin(loginObj).subscribe(resp=>{
        this.loginResponse = resp;
        if(this.loginResponse.success){
          this.haveError = false;
          this._auth.saveToken(this.loginResponse.token, this.loginResponse.user.username, this.loginResponse.user.id);
          this._router.navigate(['/dashboard']);
        }else{
          this.haveError = true;
          this.errorText = this.loginResponse.msg;
        }
      })
    }
  }

  validateLogin(loginObj){
    if(loginObj.identifier == undefined || loginObj.password == undefined){
      this.haveError = true;
      this.errorText = 'All fields are required'
      return false;
    }else{
      if(String(loginObj).length >= 8){
        this.haveError = false;
        return true;
      }else{
        this.haveError = true;
        this.errorText = 'Invalid password'
        return false;
      }
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  firstName:string;
  lastName:string;
  email:string;
  username:string;
  password:string;
  haveError:Boolean;
  errorText:string;

  signupResponse:any;
  loginResponse:any;
  constructor(private _auth:AuthService, private _router:Router) { }

  ngOnInit(): void {
    this.haveError = false;
  }

  signUp(){
    const signupObj = {
      firstName:this.firstName,
      lastName:this.lastName,
      email:this.email,
      username:this.username,
      password:this.password
    }
    if(this.validateSignup(signupObj)){
      this._auth.signup(signupObj).subscribe(response=>{
        this.signupResponse = response;
        if(this.signupResponse.success){
          this.haveError = false;
          //Login the user
          let loginObj = {
            identifier:this.email,
            password:this.password
          }
          this._auth.signin(loginObj).subscribe(resp=>{
            this.loginResponse = resp;
            if(this.loginResponse.success){
              this._auth.saveToken(this.loginResponse.token, this.loginResponse.user.username, this.loginResponse.user.id);
              this._router.navigate(['/dashboard']);
            }else{
              this._router.navigate(['/login']);
            }
          })
        }else{
          this.haveError = true;
          this.errorText = this.signupResponse.msg;
        }
      })
    }
  }

  validateSignup(signupObj:any):Boolean{
    if(signupObj.firstName == undefined || signupObj.lastName == undefined || signupObj.email == undefined || signupObj.username == undefined || signupObj.password == undefined){
      this.haveError = true;
      this.errorText = "Please fill all fields";
      return false;
    }else{
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(re.test(String(signupObj.email).toLowerCase())){
        if(String(signupObj.password).length >= 8){
          this.haveError = false;
          return true;
        }else{
          this.haveError = true;
          this.errorText = "Password must be atleast 8 characters";
          return false;
        }
      }else{
        this.haveError = true;
        this.errorText = "Please enter a valid email";
        return false;
      }
    }
  }

}

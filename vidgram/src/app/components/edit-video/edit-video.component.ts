import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { VideoListServiceService } from '../../services/video-list-service.service';
import { UploadService } from '../../services/upload.service';

@Component({
  selector: 'app-edit-video',
  templateUrl: './edit-video.component.html',
  styleUrls: ['./edit-video.component.css']
})
export class EditVideoComponent implements OnInit {
  vidId:string;
  thisVideo:any;
  private sub:any;
  vidTitle:string;
  vidDescription:string;
  vidCategory:string;
  vidTags:string;
  hasError:Boolean;
  errorText:string;
  constructor(private route:ActivatedRoute, private router:Router, private vls:VideoListServiceService, private ul:UploadService) { }

  ngOnInit(): void {
    this.hasError = false;
    this.sub = this.route.params.subscribe(params=>{
      this.vidId = params['id'];
      if(this.vidId == undefined){
        this.router.navigate(['/upload']);
      }else{
        this.vls.fetchVideoMeta(this.vidId).subscribe(response=>{
          let vidResp:any = response;
          if(vidResp.success){
            this.thisVideo = vidResp.meta;
          }else{
            this.router.navigate(['/upload']);
          }
        })
      }
    })
  }

  publishVid(){
    let tags = String(this.vidTags);
    let tagArray = tags.split(",");
    let uploadObj = {
      videoid:this.vidId,
      title:this.vidTitle,
      category:this.vidCategory,
      description:this.vidDescription,
      tags:tagArray
    }
    if(this.validatePublish(uploadObj)){
      this.ul.publishVideo(uploadObj).subscribe(resp=>{
        let pubResp:any = resp;
        if(pubResp.success){
          //Navigate to video
          this.router.navigate(['/watch',pubResp.videoid]);
        }else{
          this.hasError = true;
          this.errorText = pubResp.msg
        }
      })
    }
  }

  validatePublish(upObj):Boolean{
    if(upObj.videoid == undefined){
      this.hasError = true;
      this.errorText = 'Video ID missing. Re-upload video'
      return false
    }else{
      if(upObj.title == undefined || upObj.category == undefined){
        this.hasError = true;
        this.errorText = 'Title and category are required'
        return false;
      }else{
        this.hasError = false;
        return true;
      }
    }
  }

}

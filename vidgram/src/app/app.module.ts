import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ContentComponent } from './components/content/content.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { HomeComponent } from './components/home/home.component';
import { WatchComponent } from './components/watch/watch.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UploadComponent } from './components/upload/upload.component';

import { VideoListServiceService } from '../app/services/video-list-service.service';
import { AuthService } from '../app/services/auth.service';
import { UploadService } from '../app/services/upload.service';
import { DataService } from '../app/services/data.service';
import { AuthGuard } from '../app/guards/auth.guard';
import { EditVideoComponent } from './components/edit-video/edit-video.component';

const socketConfig:SocketIoConfig = { url:'http://localhost:8080', options:{} };

const appRoutes:Routes = [
  {
    path:'',
    component:HomeComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'signup',
    component:SignupComponent
  },
  {
    path:'dashboard',
    component:DashboardComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'upload',
    component:UploadComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'edit-video/:id',
    component:EditVideoComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'watch/:id',
    component:WatchComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ContentComponent,
    SideBarComponent,
    HomeComponent,
    WatchComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    UploadComponent,
    EditVideoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    SocketIoModule.forRoot(socketConfig),
  ],
  providers: [VideoListServiceService, AuthService, AuthGuard, UploadService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { TestBed } from '@angular/core/testing';

import { VideoListServiceService } from './video-list-service.service';

describe('VideoListServiceService', () => {
  let service: VideoListServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideoListServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

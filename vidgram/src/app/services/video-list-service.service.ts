import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VideoListServiceService {
  token: string;
  baseUrl: String = 'http://localhost:8080';
  authKey:string = 'fh67FG58jg@yUI78k';
  constructor(private http:HttpClient) { }
  
  fetchVideoMeta(videoId:String){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authKey': this.authKey
      })
    }
    return this.http.get(this.baseUrl + '/video/meta/'+videoId, httpOptions);
  }
}

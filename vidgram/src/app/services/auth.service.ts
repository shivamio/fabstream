import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  @Output() stateChange:EventEmitter<any> = new EventEmitter();

  token: string;
  user:string;
  uid:string;
  baseUrl: string = 'http://localhost:8080';
  authKey:string = 'fh67FG58jg@yUI78k';
  constructor(private _http:HttpClient) { }

  signup(signupObj){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authKey': this.authKey
      })
    }
    return this._http.post(this.baseUrl + '/users/signup', signupObj, httpOptions);
  }
  signin(SigninObj){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authKey': this.authKey
      })
    }
    return this._http.post(this.baseUrl + '/users/signin', SigninObj, httpOptions);
  }
  saveToken(token, username, uid){
    localStorage.setItem('token', token);
    localStorage.setItem('username', username);
    localStorage.setItem('uid', uid);
    this.stateChange.emit('in');
  }
  logout(){
    this.token = null;
    this.user = null;
    localStorage.clear();
    this.stateChange.emit('out');
  }
  getToken(){
    const temp = localStorage.getItem('token');
    this.token = temp;
  }
  loggedIn():Boolean{
    this.getToken();
    const helper = new JwtHelperService();
    const isExpired = helper.isTokenExpired(this.token);
    if(isExpired){
      return false;
    }else{
      return true;
    }
  }
  getUsername():string{
    const temp = localStorage.getItem('username');
    this.user = temp;
    return this.user;
  }
  getUid():string{
    const temp = localStorage.getItem('uid');
    this.uid = temp;
    return this.uid;
  }
}

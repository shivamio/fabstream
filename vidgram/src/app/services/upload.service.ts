import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpEvent, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  baseUrl: String = 'http://localhost:8080';
  authToken:string;

  constructor(private _http:HttpClient) { }

  testUpload(file:File): Observable<HttpEvent<any>>{
    let formData = new FormData();
    formData.append('videoToPublish', file);
    let params = new HttpParams();
    const options = {
      params:params,
      reportProgress:true
    }
    const req = new HttpRequest("POST", this.baseUrl+'/video/test-upload', formData, options);
    return this._http.request(req);
  }

  getToken(){
    const temp = localStorage.getItem('token');
    this.authToken = temp;
  }

  videoUpload(file:File): Observable<HttpEvent<any>>{
    this.getToken();
    let hdr = new HttpHeaders({
      'Authorization':this.authToken
    })
    let formData = new FormData();
    formData.append('videoToPublish', file);
    let params = new HttpParams();
    const options = {
      params:params,
      headers:hdr,
      reportProgress:true
    }
    const req = new HttpRequest("POST", this.baseUrl+'/video/upload', formData, options);
    return this._http.request(req);
  }

  publishVideo(pubObj){
    this.getToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.authToken
      })
    }
    return this._http.post(this.baseUrl + '/video/finish-video/', pubObj, httpOptions);
  }
}

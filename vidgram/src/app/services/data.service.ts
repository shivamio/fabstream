import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  authKey:string = 'fh67FG58jg@yUI78k';
  baseUrl: String = 'http://localhost:8080';
  constructor(private _http:HttpClient) { }
  iv(videoId:String){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authKey': this.authKey
      })
    }
    return this._http.get(this.baseUrl + '/video/uv/'+videoId, httpOptions);
  }
}

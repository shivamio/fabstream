import { TestBed } from '@angular/core/testing';

import { VideoMetaServiceService } from './video-meta-service.service';

describe('VideoMetaServiceService', () => {
  let service: VideoMetaServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideoMetaServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

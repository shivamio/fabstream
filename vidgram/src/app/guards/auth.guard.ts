import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private auth:AuthService, private router:Router){}
  canActivate(){
    if(this.auth.loggedIn()){
      console.log('user is logged in');
      return true;
    }else{
      this.router.navigate(['/login']);
      console.log('Unauthorized access attempt');
      return false;
    }
  }
}

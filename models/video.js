const mongoose = require('mongoose');
const fs = require('fs');
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath('./ffmpeg/bin/ffmpeg.exe');
ffmpeg.setFfprobePath('./ffmpeg/bin/ffprobe.exe');

const VideoSchema = mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    publisher: {
        type: String,
        required: true
    },
    file: {
        type: String,
        required: true
    },
    thumb: {
        type: String
    },
    duration: {
        type: Number,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    title: {
        type: String,
    },
    description: {
        type: String
    },
    category: {
        type: String
    },
    tags: [String],
    likes: {
        type: Number,
        default: 0
    },
    dislikes: {
        type: Number,
        default: 0
    },
    views: {
        type: Number,
        default: 0
    },
    published: {
        type: Boolean,
        default: false
    }
})

const Video = module.exports = mongoose.model('Video', VideoSchema);

module.exports.saveVideo = function (video, callback) {
    video.save(callback);
}

module.exports.finalizeVideo = function (vidId, updateObj, callback) {
    Video.findOneAndUpdate({ id: vidId }, { title: updateObj.title, category: updateObj.category, description: updateObj.description, tags: updateObj.tags, published: true }, callback);
}

module.exports.getVideosByPublisher = function (publisher, callback) {
    Video.find({ publisher: publisher }, callback);
}

module.exports.getVideoById = function (id, callback) {
    Video.findOne({ id: id }, callback);
}

module.exports.checkOwnership = function (username, videoid, callback) {
    Video.findOne({ publisher: username, id: videoid }, (err, yes) => {
        if (err) {
            console.log(err);
            callback(err, false, null);
        }
        if (yes) {
            callback(null, true, yes.file);
        } else {
            callback(null, false, null)
        }
    })
}

module.exports.deleteVideo = function (username, videoid, filename, callback) {
    Video.findOneAndDelete({ publisher: username, id: videoid }, (err, done) => {
        if (err) {
            callback(err, false);
        }
        if (done) {
            fs.unlinkSync(`./videos/${filename}`);
            fs.unlinkSync(`./thumbs/${filename}.png`);
            callback(null, true);
        } else {
            callback(null, false);
        }
    })
}

module.exports.generateThumbnail = function (filename, callback) {
    ffmpeg(`./temp/${filename}`)
        .on('filenames', function (filename) {
            console.log('Generating Thumbnail for file ' + filename)
        })
        .on('end', function () {
            ffmpeg.ffprobe(`./temp/${filename}`, (err2, metadata) => {
                console.log('Thumbnail taken, metadata captured')
                callback(null, true, metadata);
            })
        })
        .on('error', function (err) {
            console.log(err);
            callback(err, false, null);
        })
        .screenshot({
            count: 1,
            folder: './thumbs',
            size: '1280x720',
            filename: filename + '.png'
        })
}

module.exports.processVideo = function (filename, callback) {
    ffmpeg(`./temp/${filename}`)
        .videoCodec('libx264')
        .outputOptions('-crf 24')
        .on('filenames', function (filename) {
            console.log(`Processing File +${filename}`);
        })
        .on('end', function () {
            console.log('Video Processed');
            fs.unlinkSync(`./temp/${filename}`);
            console.log('Temp file deleted');
            callback(null, true);
        })
        .on('error', function (err) {
            console.log(err);
            callback(err, false);
        })
        .saveToFile(`./videos/${filename}`)
}

module.exports.getVideosByCategory = function (category, nums, callback) {
    Video.find({ category: category }, { sort: { '_id': 1 }, limit: nums }, callback);
}

module.exports.getVideosByTags = function (tags, nums, callback) {
    Video.find({ tags: { $in: tags } }, { sort: { '_id': 1 }, limit: nums }, callback);
}

module.exports.incrementView = function (videoId, callback) {
    Video.findOne({ id: videoId }, (err, video) => {
        if (err) {
            console.log('Error fetching video: ' + err);
            return callback(false);
        } else {
            if (video) {
                const v = video.views;
                const nv = v + 1;
                Video.findOneAndUpdate({ id: videoId }, { views: nv }, { new: true }, (err2, done) => {
                    if (err2) {
                        console.log('Error updating views: ' + err2);
                        return callback(false);
                    } if (done) {
                        console.log('view incremented ' + nv);
                        return callback(true)
                    }
                })
            } else {
                return callback(false);
            }
        }
    })
}
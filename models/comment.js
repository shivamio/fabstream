const mongoose = require('mongoose');
const ReplySchema = mongoose.Schema({
    user:{
        type:String,
        required:true
    },
    userDn:{
        type:String,
        required:true
    },
    text:{
        type:String,
        required:true
    }
})
const CommentSchema = mongoose.Schema({
    videoId:{
        type:String,
        required:true
    },
    user:{
        type:String,
        required:true
    },
    userDn:{
        type:String,
        required:true
    },
    text:{
        type:String,
        required:true
    },
    replies:[ReplySchema]
});

const Comment = module.exports = mongoose.model('Comment', CommentSchema);

module.exports.newComment = function(nc, callback){
    nc.save(callback);
}

module.exports.newReply = function(comId, nr, callback){
    let newReply = new ReplySchema({
        user:nr.user,
        userDn:nr.userDn,
        text:nr.text
    });
    Comment.findByIdAndUpdate(comId, {$push:{replies:newReply}}, callback);
}

module.exports.fetchComments = function(vidId, callback){
    Comment.find({videoId:vidId}, callback);
}

module.exports.deleteComment = function(comId, user, callback){
    Comment.findById(comId, (err, comment)=>{
        if(err){
            console.log('Error fetching comment for deletion: '+err);
            return callback(err, false, false);
        }if(comment){
            if(comment.user == user){
                Comment.findByIdAndDelete(comId, (err2, done)=>{
                    if(err2){
                        console.log('error deleting comment');
                        return callback(err2, true, false)
                    }else{
                        return callback(null, true, true)
                    }
                });
            }else{
                return callback(null, false, false)
            }
        }else{
            return callback('Invalid comment', false, false)
        }
    })
}

module.exports.deleteReply = function(comId, repId, callback){
    Comment.findByIdAndUpdate(comId, {$pull:{replies:{_id:repId}}}, callback);
}
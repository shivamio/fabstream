const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const UserSchema = mongoose.Schema({
    firstName: {
        type:String,
        required:true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    dp:{
        type:String,
        default:'N'
    },
    banner:{
        type:String,
        default:'N'
    },
    password: {
        type: String,
        required: true
    }
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function(id, callback){
    User.findById(id, callback);
}

module.exports.getUserByEmail = function(email, callback){
    User.findOne({email:email}, callback);
}

module.exports.getUserByUsername = function (username, callback){
    User.findOne({ username:username }, callback);
}
module.exports.checkIfEmailExist = function(email, callback){
    User.findOne({email:email}, (err, user)=>{
        if(err){
            callback(err, true);
        }
        if(user){
            callback(null, true);
        }else{
            callback(null, false);
        }
    })
}

module.exports.checkIfUsernameExist = function (username, callback) {
    User.findOne({ username: username }, (err, user) => {
        if (err) {
            callback(err, true);
        }
        if (user) {
            callback(null, true);
        } else {
            callback(null, false);
        }
    })
}

module.exports.newUser = function(newUser, callback){
    bcrypt.genSalt(10, (err, salt)=>{
        if(err){
            throw err;
        }
        bcrypt.hash(newUser.password, salt, (err2, hash)=>{
            newUser.password = hash;
            newUser.username = newUser.username.toLowerCase();
            newUser.save(callback)
        })
    })
}

module.exports.comparePassword = function (inputPass, hash, callback) {
    bcrypt.compare(inputPass, hash, (err, isMatch) => {
        if (err) {
            callback(err, false);
        } else {
            callback(null, isMatch);
        }
    })
}

module.exports.updateDp = function(id, path, callback){
    User.findByIdAndUpdate(id, {dp:path}, callback);
}
module.exports.updateBanner = function(id, path, callback){
    User.findByIdAndUpdate(id, {banner:path}, callback);
}

module.exports.getDpPath = function(username, callback){
    User.findOne({username:username}, (err, user)=>{
        if(err){
            return callback(err, false, false, null);//error, validUser, dpAvailable, dpPath
        }else{
            if(user){
                if(user.dp == 'N'){
                    return callback(null, true, false, null);
                }else{
                    return callback(null, true, true, user.dp);
                }
            }else{
                return callback(null, false, false, null);
            }
        }
    })
}
module.exports.getBannerPath = function(username, callback){
    User.findOne({username:username}, (err, user)=>{
        if(err){
            return callback(err, false, false, null);//error, validUser, bannerAvailable, bannerPath
        }else{
            if(user){
                if(user.banner == 'N'){
                    return callback(null, true, false, null);
                }else{
                    return callback(null, true, true, user.banner);
                }
            }else{
                return callback(null, false, false, null);
            }
        }
    })
}
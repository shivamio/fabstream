const express = require('express');
const router = express.Router();
const passport = require('passport');
const Comment = require('../models/comment');
const config = require('../config/database');

router.post('/new-comment', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const user = req.user._id;
    const userDn = req.user.username;
    const vidId = req.body.videoId;
    let newComment = new Comment({
        videoId: vidId,
        user: user,
        userDn: userDn,
        text: req.body.text
    });
    Comment.newComment(newComment, (err, done) => {
        if (err) {
            console.log('Error posting comment: ' + err);
            res.json({
                success: false,
                msg: 'Error occurred'
            });
        } else {
            res.json({
                success: true,
                msg: 'Comment posted'
            })
        }
    })
});
router.post('/new-reply', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const user = req.user._id;
    const userDn = req.user.username;
    const commentId = req.body.commentId;
    let newReply = {
        user: user,
        userDn: userDn,
        text: req.body.text
    };
    Comment.newReply(commentId, newReply, (err, done) => {
        if (err) {
            console.log('Error posting reply: ' + err);
            res.json({
                success: false,
                msg: 'Error occurred'
            });
        } else {
            res.json({
                success: true,
                msg: 'Reply posted'
            })
        }
    })
});

router.get('/get/:videoid', (req, res, next) => {
    const authKey = req.headers['authKey'];
    if (authKey == undefined || authKey == '') {
        res.status(400).json({
            success: false,
            msg: 'Unauthorized'
        })
    } else {
        if (authKey == config.authKey) {
            const videoId = req.params.videoid;
            Comment.fetchComments(videoId, (err, comments) => {
                if (err) {
                    console.log('Error fetching comments: ' + err);
                    res.json({
                        success: false,
                        msg: 'Error occurred'
                    })
                } if (comments) {
                    res.json({
                        success: true,
                        msg: 'Comments fetched',
                        total: comments.length,
                        comments: comments
                    })
                } else {
                    res.json({
                        success: false,
                        msg: 'Failed to fetch comments'
                    })
                }
            })
        } else {
            res.status(400).json({
                success: false,
                msg: 'Unauthorized'
            })
        }
    }
})

router.delete('/comment/:commentid', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const user = req.user._id;
    const comId = req.params.commentid;
    Comment.deleteComment(comId, user, (error, authenticated, deleted)=>{
        if(error){
            res.json({
                success:false,
                msg:'Error occurred'
            })
        }else{
            if(authenticated){
                if(deleted){
                    res.json({
                        success:true,
                        msg:'Comment deleted'
                    })
                }else{
                    res.json({
                        success:false,
                        msg:'Failed to delete comment'
                    })
                }
            }else{
                res.json({
                    success:false,
                    msg:'Unauthorized'
                })
            }
        }
    })
});

module.exports = router;
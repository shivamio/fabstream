const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const ImgUpload = require('../imgservice/upload');
const dpUpload = ImgUpload.single('dp');
const bannerUpload = ImgUpload.single('banner');
const path = require('path');

const User = require('../models/user')

router.get('/', (req, res, next) => {
    res.send('users route');
})

router.post('/signup', (req, res, next) => {
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    })
    if (newUser.firstName == undefined || newUser.firstName == '' || newUser.lastName == undefined || newUser.lastName == '' || newUser.email == undefined || newUser.email == '' || newUser.username == undefined || newUser.username == '' || newUser.password == undefined || newUser.password == '') {
        res.json({
            success: false,
            msg: 'All fields required',
            code: 102
        })
    } else {
        User.checkIfEmailExist(newUser.email, (err2, already) => {
            if (err2) {
                res.json({
                    success: false,
                    msg: 'Error occured',
                    code: 104
                })
            }
            if (already) {
                res.json({
                    success: false,
                    msg: 'Email already exists',
                    code: 103
                })
            } else {
                User.checkIfUsernameExist(newUser.username, (err3, already2) => {
                    if (err3) {
                        res.json({
                            success: false,
                            msg: 'Error occured',
                            code: 105
                        })
                    }
                    if (already2) {
                        res.json({
                            success: false,
                            msg: 'Username not available',
                            code: 106
                        })
                    } else {
                        User.newUser(newUser, (err, user) => {
                            if (err) {
                                res.json({
                                    success: false,
                                    msg: 'Error occured',
                                    code: 101
                                })
                            } else {
                                res.json({
                                    success: true,
                                    msg: 'Signup complete',
                                    code: 1
                                })
                            }
                        })
                    }
                })
            }
        })
    }
})

router.post('/signin', (req, res, next) => {
    const identifier = req.body.identifier;
    const password = req.body.password;
    if (identifier == undefined || identifier == '' || password == undefined || password == '') {
        res.json({
            success: false,
            msg: 'All fields required',
            code: 110
        })
    } else {
        User.getUserByEmail(identifier, (err, user) => {
            if (err) {
                res.json({
                    success: false,
                    msg: 'Error occured',
                    code: 107
                })
            }
            if (user) {
                User.comparePassword(password, user.password, (error, isMatch) => {
                    if (error) {
                        res.json({
                            success: false,
                            msg: 'Error occured',
                            code: 111
                        })
                    } if (isMatch) {
                        const token = jwt.sign({ data: user }, config.secret, {
                            expiresIn: 604800 // Expires in a week
                        });
                        res.json({
                            success: true,
                            token: 'Bearer ' + token,
                            user: {
                                id: user._id,
                                firstName: user.firstName,
                                lastName: user.lastName,
                                username: user.username,
                                email: user.email
                            },
                            code: 2
                        })
                    } else {
                        res.json({
                            success: false,
                            msg: 'Invalid Credentials',
                            code: 112
                        })
                    }
                })
            } else {
                User.getUserByUsername(identifier, (err2, user2) => {
                    if (err2) {
                        res.json({
                            success: false,
                            msg: 'Error occured',
                            code: 108
                        })
                    }
                    if (user2) {
                        User.comparePassword(password, user2.password, (error, isMatch) => {
                            if (error) {
                                res.json({
                                    success: false,
                                    msg: 'Error occured',
                                    code: 111
                                })
                            } if (isMatch) {
                                const token = jwt.sign({ data: user2 }, config.secret, {
                                    expiresIn: 604800 // Expires in a week
                                });
                                res.json({
                                    success: true,
                                    token: 'Bearer ' + token,
                                    user: {
                                        id: user2._id,
                                        firstName: user2.firstName,
                                        lastName: user2.lastName,
                                        username: user2.username,
                                        email: user2.email
                                    },
                                    code: 2
                                })
                            } else {
                                res.json({
                                    success: false,
                                    msg: 'Invalid Credentials',
                                    code: 112
                                })
                            }
                        })
                    } else {
                        res.json({
                            success: false,
                            msg: 'Invalid Credentials',
                            code: 112
                        })
                    }
                })
            }
        })
    }
})

router.get('/profile', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.json({
        success: true,
        user: req.user,
        msg: 'You accessed profile',
        code: 3
    })
});

router.post('/upload-dp', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    dpUpload(req, res, (err) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Error Occured'
            })
        } else {
            const imgPath = 'img/' + req.file.filename;
            User.updateDp(req.user._id, imgPath, (err2, done) => {
                if (err2) {
                    console.log('Path save error: ' + err2);
                    res.json({
                        success: false,
                        msg: 'Failed to upload'
                    })
                } else {
                    res.json({
                        success: true,
                        msg: 'DP updated'
                    })
                }
            })
        }
    })
});

router.post('/upload-banner', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    bannerUpload(req, res, (err) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Error Occured'
            })
        } else {
            const imgPath = 'img/' + req.file.filename;
            User.updateBanner(req.user._id, imgPath, (err2, done) => {
                if (err2) {
                    console.log('Path save error: ' + err2);
                    res.json({
                        success: false,
                        msg: 'Failed to upload'
                    })
                } else {
                    res.json({
                        success: true,
                        msg: 'Banner updated'
                    })
                }
            })
        }
    })
});

router.get('/publisher-img/:username', (req, res, next) => {
    const username = req.params.username;
    User.getDpPath(username, (error, validUser, dpAvailable, dpPath) => {
        if (error) {
            console.log('Error fetching dp path: ' + error);
            res.status(500).json({
                success: false,
                msg: 'Errror occurred'
            });
        } else {
            if (validUser) {
                if (dpAvailable) {
                    res.status(200).sendFile(path.join(__dirname, '../', dpPath));
                } else {
                    res.status(404).json({
                        success: false,
                        msg: 'DP not found'
                    })
                }
            } else {
                res.status(404).json({
                    success: false,
                    msg: 'User not found'
                })
            }
        }
    })
})

router.get('/dp/:username', (req, res, next) => {
    const authKey = req.header('authKey');
    console.log(authKey);
    if (authKey == undefined || authKey == '') {
        res.status(401).json({
            success: false,
            msg: 'Unauthorized'
        })
    } else {
        if (authKey == config.authKey) {
            const username = req.params.username;
            User.getDpPath(username, (error, validUser, dpAvailable, dpPath) => {
                if (error) {
                    console.log('Error fetching dp path: ' + error);
                    res.status(500).json({
                        success: false,
                        msg: 'Errror occurred'
                    });
                } else {
                    if (validUser) {
                        if (dpAvailable) {
                            res.status(200).sendFile(path.join(__dirname, '../', dpPath));
                        } else {
                            res.status(404).json({
                                success: false,
                                msg: 'DP not found'
                            })
                        }
                    } else {
                        res.status(404).json({
                            success: false,
                            msg: 'User not found'
                        })
                    }
                }
            })
        } else {
            res.status(401).json({
                success: false,
                msg: 'Unauthorized'
            })
        }
    }
});
router.get('/banner/:username', (req, res, next) => {
    const authKey = req.header('authKey');
    console.log(authKey);
    if (authKey == undefined || authKey == '') {
        res.status(401).json({
            success: false,
            msg: 'Unauthorized'
        })
    } else {
        if (authKey == config.authKey) {
            const username = req.params.username;
            User.getBannerPath(username, (error, validUser, bannerAvailable, bannerPath) => {
                if (error) {
                    console.log('Error fetching dp path: ' + error);
                    res.status(500).json({
                        success: false,
                        msg: 'Errror occurred'
                    });
                } else {
                    if (validUser) {
                        if (bannerAvailable) {
                            res.status(200).sendFile(path.join(__dirname, '../', bannerPath));
                        } else {
                            res.status(404).json({
                                success: false,
                                msg: 'Banner not found'
                            })
                        }
                    } else {
                        res.status(404).json({
                            success: false,
                            msg: 'User not found'
                        })
                    }
                }
            })
        } else {
            res.status(401).json({
                success: false,
                msg: 'Unauthorized'
            })
        }
    }
});

module.exports = router;
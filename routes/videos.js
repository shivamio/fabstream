const express = require('express');
const router = express.Router();
const random = require('crypto-random-string');
const passport = require('passport');
const dateTime = require('date-and-time');
const multer = require('multer');
const Video = require('../models/video');
const User = require('../models/user');
const fs = require('fs');
const path = require('path');
const config = require('../config/database');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './temp');
    },
    filename: function (req, file, cb) {
        var fileName = random({ length: 30, type: 'url-safe' })
        cb(null, fileName + '.mp4')
    }
})
const upload = multer({ storage: storage })

router.post('/upload', passport.authenticate('jwt', { session: false }), upload.single('videoToPublish'), (req, res, next) => {
    console.log(req.file);
    const now = new Date();
    const timestamp = dateTime.format(now, 'MMM DD YYYY');
    Video.generateThumbnail(req.file.filename, (err, done, metadata) => {
        if (err) {
            console.log(err);
            res.json({
                success: false,
                msg: 'Error occured'
            })
        }
        if (done) {
            //Process video
            Video.processVideo(req.file.filename, (err3, processed) => {
                if (err3) {
                    console.log(err3);
                }
                if (processed) {
                    req.app.io.emit(req.user._id, { event: 'video-process-complete' });
                    console.log('Video processed and saved in videos')
                } else {
                    console.log('Failed to process file')
                }
            })
            let thisVideo = new Video({
                id: random({ length: 16, type: 'url-safe' }),
                publisher: req.user.username,
                file: req.file.filename,
                duration: metadata.format.duration,
                date: timestamp
            })
            Video.saveVideo(thisVideo, (err4, videoSaved) => {
                if (err4) {
                    console.log(err4);
                    res.json({
                        success: false,
                        msg: 'Error occured',
                        code: 113
                    })
                }
                if (videoSaved) {
                    req.app.io.emit(req.user._id, { event: 'video-process-start', videoId: thisVideo.id });
                    res.json({
                        success: true,
                        msg: 'File uploaded, Processing in progress',
                        file: thisVideo,
                        code: 4
                    })
                }
            })
        } else {
            res.json({
                success: false,
                msg: 'Failed to generate thumbnail'
            })
        }
    })
});

router.post('/finish-video', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const username = req.user.username;
    const videoId = req.body.videoid;
    Video.checkOwnership(username, videoId, (err0, yes, filename) => {
        if (err0) {
            console.log(err0);
            res.json({
                success: false,
                msg: 'Error occured',
                code: 119
            })
        } if (yes) {
            let updateObj = {
                title: req.body.title,
                category: req.body.category,
                description: req.body.description,
                tags: req.body.tags
            }
            Video.finalizeVideo(videoId, updateObj, (err, done) => {
                if (err) {
                    console.log(err);
                    res.json({
                        success: false,
                        msg: 'Failed to publish video'
                    })
                } if (done) {
                    res.json({
                        success: true,
                        msg: 'Video published',
                        videoid: done.id
                    })
                }
            })
        } else {
            res.json({
                success: false,
                msg: 'Unauthorized',
                code: 120
            })
        }
    })
})

router.delete('/:videoid', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const username = req.user.username;
    const videoId = req.params.videoid;
    Video.checkOwnership(username, videoId, (err0, yes, filename) => {
        if (err0) {
            console.log(err0);
            res.json({
                success: false,
                msg: 'Error occured',
                code: 119
            })
        }
        if (yes) {
            Video.deleteVideo(username, videoId, filename, (err2, done) => {
                if (err2) {
                    console.log(err2);
                    res.json({
                        success: false,
                        msg: 'Error occured',
                        code: 121
                    })
                }
                if (done) {
                    res.json({
                        success: true,
                        msg: 'Video deleted',
                        code: 10
                    })
                } else {
                    res.json({
                        success: false,
                        msg: 'Error occured',
                        code: 122
                    })
                }
            })
        } else {
            res.json({
                success: false,
                msg: 'Unauthorized',
                code: 120
            })
        }
    })
})

router.get('/myvideos', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    //Publisher's own videos
    Video.getVideosByPublisher(req.user.username, (err, videos) => {
        if (err) {
            console.log(err);
            res.json({
                success: false,
                msg: 'Error occured',
                code: 118
            })
        }
        if (videos.length > 0) {
            res.json({
                success: true,
                msg: 'Videos fetched',
                videos: videos,
                total: videos.length,
                owner: true,
                code: 8
            })
        } else {
            res.json({
                success: true,
                msg: 'No videos found',
                total: 0,
                code: 9
            })
        }
    })
})

router.get('/bypublisher/:publisher', (req, res, next) => {
    //Get Videos for a particular publisher
    const publisher = req.params.publisher;
    User.checkIfUsernameExist(publisher, (err2, yes) => {
        if (err2) {
            console.log(err2);
            res.json({
                success: false,
                msg: 'Error occured',
                code: 117
            })
        }
        if (yes) {
            Video.getVideosByPublisher(publisher, (err, videos) => {
                if (err) {
                    console.log(err);
                    res.json({
                        success: false,
                        msg: 'Error occured',
                        code: 116
                    })
                }
                if (videos.length > 0) {
                    res.json({
                        success: true,
                        msg: 'Videos fetched',
                        videos: videos,
                        total: videos.length,
                        code: 6
                    })
                } else {
                    res.json({
                        success: true,
                        msg: 'No videos found',
                        total: 0,
                        code: 7
                    })
                }
            })
        }
        else {
            res.json({
                success: false,
                msg: 'Invalid publisher id',
                code: 117
            })
        }
    })
})

router.get('/meta/:videoid', (req, res, next) => {
    const authKey = req.header('authKey');
    if (authKey == undefined || authKey == '') {
        res.status(400).json({
            success: false,
            msg: 'Unauthorized'
        })
    } else {
        if (authKey == config.authKey) {
            const videoId = req.params.videoid;
            if (videoId == undefined || videoId == '') {
                res.json({
                    success: false,
                    msg: 'Invalid id'
                })
            } else {
                Video.getVideoById(videoId, (err, video) => {
                    if (err) {
                        console.log(err);
                        res.json({
                            success: false,
                            msg: 'Error occured',
                            code: 114
                        })
                    }
                    if (video) {
                        res.json({
                            success: true,
                            msg: 'Video Meta fetched',
                            meta: {
                                id: video.id,
                                publisher: video.publisher,
                                thumbnail: `http://localhost:8080/video/thumb/${video.id}`,
                                stream: `http://localhost:8080/video/stream/${video.id}`,
                                duration: video.duration,
                                date: video.date,
                                title: video.title,
                                description: video.description,
                                category: video.category,
                                tags: video.tags,
                                views: video.views
                            },
                            code: 5
                        })
                    }
                    else {
                        res.json({
                            success: false,
                            msg: 'Video not found',
                            code: 115
                        })
                    }
                })
            }
        } else {
            res.status(400).json({
                success: false,
                msg: 'Unauthorized'
            })
        }
    }
})

router.get('/uv/:videoid', (req,res,next)=>{
    const authKey = req.header('authKey');
    if (authKey == undefined || authKey == '') {
        res.status(400).json({
            success: false,
            msg: 'Unauthorized'
        })
    } else {
        if (authKey == config.authKey) {
            Video.incrementView(req.params.videoid, (done)=>{
                if(done){
                    res.json({
                        success:true,
                        msg:'View incremented'
                    })
                }else{
                    res.json({
                        success:false,
                        msg:'Failed to increment view'
                    })
                }
            })
        }else{
            res.status(400).json({
                success: false,
                msg: 'Unauthorized'
            })
        }
    }
})

router.get('/stream/:videoid', (req, res, next) => {
    const videoId = req.params.videoid;
    let viewUpdated = false;
    if (videoId == undefined || videoId == '') {
        res.json({
            success: false,
            msg: 'Invalid id'
        })
    } else {
        Video.getVideoById(videoId, (err, video) => {
            if (err) {
                console.log(err);
                res.json({
                    success: false,
                    msg: 'Error occured',
                    code: 114
                })
            }
            if (video) {
                const path = `./videos/${video.file}`;
                const stat = fs.statSync(path);
                const fileSize = stat.size;
                const range = req.headers.range;
                if (range) {
                    const parts = range.replace(/bytes=/, "").split("-")
                    const start = parseInt(parts[0], 10)
                    const end = parts[1]
                        ? parseInt(parts[1], 10)
                        : fileSize - 1
                    const chunksize = (end - start) + 1
                    const file = fs.createReadStream(path, { start, end })
                    const head = {
                        'Content-Range': `bytes ${start}-${end}/${fileSize}`,
                        'Accept-Ranges': 'bytes',
                        'Content-Length': chunksize,
                        'Content-Type': 'video/mp4',
                    }
                    res.writeHead(206, head);
                    file.pipe(res);
                } else {
                    const head = {
                        'Content-Length': fileSize,
                        'Content-Type': 'video/mp4',
                    }
                    res.writeHead(200, head)
                    fs.createReadStream(path).pipe(res)
                }
            }
            else {
                res.json({
                    success: false,
                    msg: 'Video not found',
                    code: 115
                })
            }
        })
    }
})


router.get('/thumb/:videoid', (req, res, next) => {
    const videoId = req.params.videoid;
    if (videoId == undefined || videoId == '') {
        res.json({
            success: false,
            msg: 'Invalid id'
        })
    } else {
        Video.getVideoById(videoId, (err, video) => {
            if (err) {
                console.log(err);
                res.json({
                    success: false,
                    msg: 'Error occured',
                    code: 114
                })
            }
            if (video) {
                res.status(200).sendFile(path.join(__dirname, '../', `thumbs/${video.file}.png`));
            }
            else {
                res.json({
                    success: false,
                    msg: 'Video not found',
                    code: 115
                })
            }
        })
    }
});

router.post('/test-upload', upload.single('videoToPublish'), (req, res, next) => {
    console.log(req.file);
    res.json({
        success: true,
        msg: 'File upload started'
    })
})

module.exports = router;
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

//Connect to mongoDB
mongoose.connect(config.database, {useNewUrlParser:true, useUnifiedTopology:true});
mongoose.connection.on('connected',()=>{
    console.log('Connected to database '+config.database);
})
mongoose.connection.on('error', (err) => {
    console.log('Database connection error ' + err);
})


const app = express();

//Socket.io stuff
var server = require('http').createServer(app);
var io = require('socket.io')(server);

//Route files
const users = require('./routes/users');
const videos = require('./routes/videos');
const comments = require('./routes/comment');

const port = process.env.PORT || 8080;

app.use(express.static(path.join(__dirname,'public')));

//Middlewares
app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

app.get('/', (req,res)=>{
    res.send('invalid endpoint');
})

app.use('/users', users);
app.use('/video', videos);
app.use('/comments', comments);

//Socket.io stuff
app.io = io;

io.on('connection', function(socket){
    console.log('User connected');
})

server.listen(port, ()=>{
    console.log('Server started on port: '+port)
});